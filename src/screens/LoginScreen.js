import React, { useState, useContext } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Title } from 'react-native-paper';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import { AuthContext } from '../navigation/AuthProvider';

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { login } = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <Title style={styles.titleText}>
        <Image source={{
          uri:"https://img.favpng.com/17/1/20/user-profile-computer-icons-login-png-favpng-w4PDSTJJhQAyy1K6Dbdyjx4XW.jpg", width: 130, height: 130}}>
        </Image>
      </Title>
      <FormInput style={styles.form_input}
        labelName='Email'
        value={email}
        autoCapitalize='none'
        onChangeText={userEmail => setEmail(userEmail)}
      />
      <FormInput style={styles.form_input_pw}
        labelName='Password'
        value={password}
        secureTextEntry={true}
        onChangeText={userPassword => setPassword(userPassword)}
      />
      <FormButton style={styles.actionBTN}
        title='Login'
        modeValue='contained'
        labelStyle={styles.loginButtonLabel}
        onPress={() => login(email, password)}
      />
      <FormButton
        title='Sign Up? Join here'
        modeValue='text'
        uppercase={false}
        labelStyle={styles.navButtonText}
        onPress={() => navigation.navigate('Signup')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  form_input:{
    width: 360,
  },
  form_input_pw:{
    width: 360,
    marginTop: 15
  },
  titleText: {
    fontSize: 32,
    marginBottom: 25,
    color: '#590995'
  },
  loginButtonLabel: {
    fontSize: 18,
    marginRight: 0,
    textAlign: 'center'
  },
  navButtonText: {
    fontSize: 16,
    marginTop: -40
  }
});