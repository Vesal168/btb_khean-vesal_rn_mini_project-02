import React, {Component, useContext} from 'react';
import {View, Text, SafeAreaView, Image} from 'react-native';
import { IconButton } from 'react-native-paper';
import { AuthContext } from '../navigation/AuthProvider';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userProfile: '',
    };
  }

goBack = (navigation) => {
    this.props.navigation.navigate('ChatApp')
}

  

  render() {

    return (
      <SafeAreaView>
        <View style={{alignItems: 'center', marginTop: 20}}>
            <Image
              source={{
                uri:
                  'https://img.favpng.com/17/1/20/user-profile-computer-icons-login-png-favpng-w4PDSTJJhQAyy1K6Dbdyjx4XW.jpg',
                width: 120,
                height: 120,
              }}>
            </Image>
          </View>
          <View style={{alignItems: 'center', marginTop: 15}}>
          <Text style={{fontSize: 20, fontWeight: '400'}}> sal@gmail.com </Text>
          <Text style={{fontSize: 16, fontWeight: '300'}}> Google Service Co.Ltd </Text>
          </View>

          <View style={{marginTop: 30, padding: 20}}>
              <Text style={{fontSize: 20, fontWeight: '400'}}>Work Experience</Text>
              <Text style={{marginTop: 15, fontSize: 18, alignContent: 'stretch'}}>I used to work as web developer at Localize Company, My responsibility for my task there as Angular Framework. And I used to work with laravel framework also.</Text>
          </View>
          <View style={{marginTop: 10, padding: 20}}>
              <Text style={{fontSize: 20, fontWeight: '400'}}>Contact me on Social</Text>
              <Text style={{marginTop: 15, fontSize: 18, alignContent: 'stretch'}}>
                   <View style={{flexDirection: 'row'}}>
                        <IconButton icon ='facebook' size={40}/>
                        <IconButton icon ='instagram' size={40}/>
                        <IconButton icon ='twitter' size={40}/>
                   </View>
              </Text>
          </View>
          <View style={{marginTop: 10, padding: 20}}>
              <Text style={{fontSize: 20, fontWeight: '400'}}>Back To Home</Text>
              <Text style={{marginTop: 15, fontSize: 18, alignContent: 'stretch'}}>
                   <View style={{flexDirection: 'row'}}>
                        <IconButton icon ='arrow-left-circle' size={40} onPress={this.goBack} />
                   </View>
              </Text>
          </View>
      </SafeAreaView>
    );
  }
}
