import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { IconButton } from 'react-native-paper';
import HomeScreen from '../screens/HomeScreen';
import AddRoomScreen from '../screens/AddRoomScreen';
import RoomScreen from '../screens/RoomScreen';
import GroupChat from '../screens/GroupChat';
import { AuthContext } from './AuthProvider';
import Profile from '../components/Profile';
import LoginScreen from '../screens/LoginScreen';
import { Platform, StyleSheet, Text, View } from "react-native";


const ChatAppStack = createStackNavigator();
const ModalStack = createStackNavigator();
const styles = StyleSheet.create({
  iconContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: 120
  }
});
/**
 * All chat app related screens
 */

function ChatApp() {
  const { logout } = useContext(AuthContext);
  return (
    <ChatAppStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#db6400'
        },
        headerTintColor: '#ffffff',
        headerTitleStyle: {
          fontSize: 22
        }
      }}
    >
      <ChatAppStack.Screen
        name='Home'
        component={HomeScreen}
        options={({ navigation }) => ({
          headerRight: () => (
            <View style = {styles.iconContainer}>
              <IconButton
              icon='plus'
              size={28}
              color='#ffffff'
              onPress={() => navigation.navigate('AddRoom')}
              />
              <IconButton style={{marginLeft: -20}}
              icon='account-group'
              size={28}
              color='#ffffff'
              onPress={() => navigation.navigate('GroupChat')}
              />
            </View>
          ),
          headerLeft: () => (
            <View style = {styles.iconContainer}>
              <IconButton
              icon='logout-variant'
              size={28}
              color='#ffffff'
              onPress={() => logout()}
              />
              <IconButton style={{marginLeft: -15}}
              icon='account-circle-outline'
              size={28}
              color='#ffffff'
              onPress={() => navigation.navigate('Profile')}
            />
            </View>
          ),
        })}
      />
      <ChatAppStack.Screen
        name='Room'
        component={RoomScreen}
        options={({ route }) => ({
          title: route.params.thread.name
        })}
      />
    </ChatAppStack.Navigator>
  );
}



export default function HomeStack() {
  return (
    <ModalStack.Navigator mode='modal' headerMode='none'>
      <ModalStack.Screen name='ChatApp' component={ChatApp} />
      <ModalStack.Screen name='Profile' component={Profile} />
      <ModalStack.Screen name="GroupChat" component={GroupChat}/>
      <ModalStack.Screen name='AddRoom' component={AddRoomScreen} />
    </ModalStack.Navigator>
  );
}